//
//  CalculatorModel.swift
//  Calculator Swift
//
//  Created by IIMJobs User on 13/08/17.
//  Copyright © 2017 14K. All rights reserved.
//

import Foundation

class CalculatorModel{
    
    private enum Op {
        
        case Operand(Double)
        case UnaryOperation(String, (Double)->Double)
        case BinaryOperation(String, (Double,Double)->Double)
        
        
        var description : String{
            get{
                switch self {
                case .Operand(let operand):
                    return "\(operand)"
                case .BinaryOperation(let symbol, _):
                    return symbol
                case .UnaryOperation(let symbol, _):
                    return symbol
                }
            }
        }
        
    }
    
    private var operationStack = [Op]()
    
    //var knownOps = Dictionary<String,Op>()
    private var knownOps = [String:Op]()
    
    init() {
        knownOps["➕"] = Op.BinaryOperation("➕", +) //Uses the infix method
        
        knownOps["➖"] = Op.BinaryOperation("➖", { (op1 , op2) in
            return op1-op2
        })
        
        knownOps["➗"] = Op.BinaryOperation("➗", { (op1 , op2) in
            return op1/op2
        })
        
        knownOps["✖️"] = Op.BinaryOperation("✖️", { (op1 , op2) in
            return op1*op2
        })
        
        knownOps["⌡"] = Op.UnaryOperation("⌡", sqrt)
        
    }
    
    
    private func evaluate(ops : [Op]) -> (result: Double? , remainingStack: [Op]) {
        
        if(!ops.isEmpty)
        {
            var remainingOps = ops
            let op = remainingOps.removeLast()
            
            switch op {
            case Op.Operand(let operand):
                return (operand,remainingOps)
                
            case Op.UnaryOperation(_, let operation):
                let operandEvauation = evaluate(ops: remainingOps)
                if let operand = operandEvauation.result{
                    return (operation(operand), operandEvauation.remainingStack)
                }
                break
            case Op.BinaryOperation(_, let operation):
                let operationEvaluation = evaluate(ops:remainingOps)
                if let operand = operationEvaluation.result{
                    let operationEvaluation2 = evaluate(ops : operationEvaluation.remainingStack)
                    if let operand2 = operationEvaluation2.result {
                        return (operation(operand,operand2),operationEvaluation2.remainingStack)
                    }
                }
            }
        }
        return (nil,ops)
    }
    
    func evaluate() -> Double?{
        let (result , remainder) = evaluate(ops: operationStack)
        print("\(operationStack) = \(result) with \(remainder) left over")
        return result;
    }
    
    func pushOperand(operand:Double) -> Double? {
        operationStack.append(Op.Operand(operand));
        return evaluate()
    }
    
    func performOperations(symbol:String) -> Double?{
        if let operation = knownOps[symbol] {
            operationStack.append(operation)
        }
        return evaluate()
    }
    
}

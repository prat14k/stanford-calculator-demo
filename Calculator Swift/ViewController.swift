//
//  ViewController.swift
//  Calculator Swift
//
//  Created by IIMJobs User on 12/08/17.
//  Copyright © 2017 14K. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var resultLabel: UILabel!
    
    var isTyping = false
    
    @IBAction func digitAction(_ sender: UIButton) {
        
        
        let digit = sender.currentTitle!
        print("Digit : \(digit)")
        
        if(isTyping){
            resultLabel.text = resultLabel.text! + digit
        }
        else{
            resultLabel.text = digit
            isTyping = true
        }
        
    }
    
    var calciModel = CalculatorModel()
    
    @IBAction func operateAction(_ sender: UIButton) {
        
        
        if isTyping {
            enterAction()
        }
        
        if let operation = sender.currentTitle{
            if let result = calciModel.performOperations(symbol: operation){
                displayValue = result
            }
            else{
                displayValue = 0
            }
        }
    }
    
    @IBAction func enterAction() {
        
        isTyping = false
        print("enter Action")
        
        if let result = calciModel.pushOperand(operand: displayValue)
        {
            displayValue = result
        }
        else{
            displayValue = 0
    
        }
    }
    var displayValue : Double{
        get{
            return Double(resultLabel.text!)!
        }
        set{
            resultLabel.text = "\(newValue)"
            isTyping = false
        }
    }
    

}

